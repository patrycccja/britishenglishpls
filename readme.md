# Number translator

The app just reads lines from the System input, takes any provided numbers
(from -999,999,999 up to 999,999,999) and transforms them to british english words.

Eg.

```
Type a number you want to be transformed to british english words (only values between -999,999,999 and 999,999,999 are translated)
OR
type anything else to cleanly exit the service.
> 0
Equivalent: zero
> 123456
Equivalent: one hundred and twenty three thousand four hundred and fifty six
> -989545121
Equivalent: minus nine hundred and eighty nine million five hundred and forty five thousand one hundred and twenty one
> q
Thank you. Have a nice day!
```

## RUN THE APP

Just run the Main.main() method. Or:

```
mvn clean package
```
and then
```
java -jar britishenglishpls-1.0-SNAPSHOT.jar
```

Done :) No params are needed as the app will prompt you to provide some numbers.