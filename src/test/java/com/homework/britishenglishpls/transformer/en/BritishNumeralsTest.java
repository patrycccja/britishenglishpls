package com.homework.britishenglishpls.transformer.en;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class BritishNumeralsTest {
    private BritishNumerals bn = new BritishNumerals();

    @Test
    public void testAll() {
        for (int i=0; i<100; i++) {
            assertNotNull(bn.getNumeral(i));
        }
    }

    @Test
    public void testRandomValues() {
        assertEquals("zero", bn.getNumeral(0));
        assertEquals("thirteen", bn.getNumeral(13));
        assertEquals("seventy three", bn.getNumeral(73));
        assertEquals("seventy seven", bn.getNumeral(77));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegative() {
        bn.getNumeral(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooBig() {
        bn.getNumeral(100);
    }

}
