package com.homework.britishenglishpls.transformer.en;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BritishNumberTransformerTest {
    BritishNumberTransformer bnt = new BritishNumberTransformer();

    @Test(expected = IllegalArgumentException.class)
    public void testRangeMax() {
        bnt.transform(1000000000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRangeMin() {
        bnt.transform(-1000000000);
    }

    @Test
    public void testZero() {
        assertEquals("zero", bnt.transform(0));
    }

    @Test
    public void testMinMax() {
        assertEquals("minus " + bnt.transform(999999999), bnt.transform(-999999999));
    }

    @Test
    public void testRandomValues() {
        assertEquals("one", bnt.transform(1));
        assertEquals("twenty one", bnt.transform(21));
        assertEquals("one hundred and five", bnt.transform(105));
        assertEquals("seven million", bnt.transform(7000000));
        assertEquals("seven million and seven thousand", bnt.transform(7007000));
        assertEquals("seven million and seven hundred", bnt.transform(7000700));
        assertEquals("seven million and seven", bnt.transform(7000007));
        assertEquals("seven million seven thousand and seven", bnt.transform(7007007));
        assertEquals("fifty six million nine hundred and forty five thousand seven hundred and eighty one", bnt.transform(56945781));
        assertEquals("minus fifty six million nine hundred and forty five thousand seven hundred and eighty one",
                bnt.transform(-56945781));
    }
}