package com.homework.britishenglishpls;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import com.homework.britishenglishpls.transformer.NumberTransformer;
import com.homework.britishenglishpls.transformer.NumberTransformerFactory;

/**
 * The Java entry point. This method reads lines from the System input, takes any provided numbers
 * (from -999,999,999 up to 999,999,999) and transforms them to british english words.
 */
public class Main {

    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in, StandardCharsets.UTF_8.name());
        System.out.println("Type a number you want to be transformed to british english words (only values between " +
                "-999,999,999 and 999,999,999 are translated)");
        System.out.println("OR");
        System.out.println("type anything else to cleanly exit the service.");
        System.out.print("> ");
        int number;
        NumberTransformer transformer = NumberTransformerFactory.getTransformer(NumberTransformerFactory.LANG.EN);
        while (sc.hasNext()) {
            if (!sc.hasNextInt()) {
                System.out.println("Thank you. Have a nice day!");
                break;
            }
            try {
                number = Integer.parseInt(sc.nextLine());
                System.out.println("Equivalent: " + transformer.transform(number));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
            System.out.print("> ");
        }
        sc.close();
    }

}
