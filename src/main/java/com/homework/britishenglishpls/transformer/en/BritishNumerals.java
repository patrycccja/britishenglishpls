package com.homework.britishenglishpls.transformer.en;

import java.util.HashMap;

public class BritishNumerals {
    private static HashMap<Integer, String> numerals = new HashMap<>();

    static {
        numerals.put(0, "zero");
        numerals.put(1, "one");
        numerals.put(2, "two");
        numerals.put(3, "three");
        numerals.put(4, "four");
        numerals.put(5, "five");
        numerals.put(6, "six");
        numerals.put(7, "seven");
        numerals.put(8, "eight");
        numerals.put(9, "nine");
        numerals.put(10, "ten");
        numerals.put(11, "eleven");
        numerals.put(12, "twelve");
        numerals.put(13, "thirteen");
        numerals.put(14, "fourteen");
        numerals.put(15, "fifteen");
        numerals.put(16, "sixteen");
        numerals.put(17, "seventeen");
        numerals.put(18, "eighteen");
        numerals.put(19, "nineteen");
        numerals.put(20, "twenty");
        numerals.put(30, "thirty");
        numerals.put(40, "forty");
        numerals.put(50, "fifty");
        numerals.put(60, "sixty");
        numerals.put(70, "seventy");
        numerals.put(80, "eighty");
        numerals.put(90, "ninety");
    }

    /**
     * Returns corresponding literal for numbers from 0 to 99.
     * @throws java.lang.IllegalArgumentException if provided number is not between 0 and 99.
     */
    public static String getNumeral(int number) throws IllegalArgumentException {
        if (number<0 || number>99) {
            throw new IllegalArgumentException("Provided number can not be translated to a numeral. It's smaller than 0 or " +
                    "bigger than 99.");
        }
        if (numerals.containsKey(number))
            return numerals.get(number);
        return numerals.get(number - number%10) + " " + numerals.get(number%10);
    }

}
