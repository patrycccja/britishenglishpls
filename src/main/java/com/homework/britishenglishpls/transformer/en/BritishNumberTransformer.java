package com.homework.britishenglishpls.transformer.en;

import java.util.ArrayList;

import com.homework.britishenglishpls.transformer.NumberTransformer;

/**
 * Translates numbers (from -999,999,999 up to 999,999,999) to british number words.
 */
public class BritishNumberTransformer implements NumberTransformer {

    public String transform(int inputNumber) throws IllegalArgumentException {
        if (inputNumber == 0) {
            return "zero";
        }
        if (inputNumber < -999999999 || inputNumber > 999999999) {
            throw new IllegalArgumentException("Provided number is too big. Only values between " +
                    "-999,999,999 and 999,999,999 are accepted.");
        }
        ArrayList<Segment> segments = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        int number = inputNumber;
        if (number < 0) {
            sb.append("minus ");
            number *= -1;
        }
        // crate "segments" = triplets of digits, eg. 1234 -> {1, 234}
        // (except segments has it defined in the reverse order: {234, 1})
        int smallNum;
        boolean soFarOnlyZeroes = true;
        while (number > 0) {
            smallNum = number % 1000;
            number = (number - smallNum) / 1000;
            segments.add( new Segment(smallNum, soFarOnlyZeroes) );
            if (smallNum!=0) {
                soFarOnlyZeroes = false;
            }
        }
        // transform each token to create translation + add "and" where necessary
        String res;
        for (int i=segments.size()-1; i>=0; i--) {
            res = transform3digitNumber(segments.get(i).getValue());
            if ( !res.equals("") ) {
                if (sb.length()>0 && segments.get(i).isLast() && !res.contains(" and ")) {
                    sb.append("and "); // => for numbers like 1000700 additional "and" is added before "seven hundred"
                }
                sb.append(res).append(" ");
                if (i == 2) {
                    sb.append("million ");
                } else if (i == 1) {
                    sb.append("thousand ");
                }
            }
        }
        return sb.toString().trim();
    }

    /** Takes the provided number (from 0 up to 999) and returns the equivalent number in british english words. For 0 returns
     * an empty string.
     */
    private String transform3digitNumber(int number) {
        if (number == 0) {
            return "";
        }
        String res = "";
        int hundreds = number / 100;
        int rest = number % 100;
        if (hundreds > 0) {
            res += BritishNumerals.getNumeral(hundreds) + " hundred ";
            if (rest > 0) {
                res += "and ";
            }
        }
        if (rest > 0) {
            res += BritishNumerals.getNumeral(rest);
        }
        return res;
    }

    private class Segment {
        private int value;
        private boolean last; //true means this is the last segment with value>0

        private Segment(int value, boolean last) {
            this.last = last;
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public boolean isLast() {
            return last;
        }
    }

}
