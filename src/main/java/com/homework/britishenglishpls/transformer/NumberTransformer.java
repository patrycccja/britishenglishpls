package com.homework.britishenglishpls.transformer;

/**
 * Translates numbers (from -999,999,999 up to 999,999,999) to number words.
 */
public interface NumberTransformer {
    /** Takes the provided number and returns the equivalent number in british english words.
     * Eg. 56945781 will be transformed to "fifty six million nine hundred and forty five thousand seven hundred and eighty one".
     *
     * @param inputNumber number to be translated
     * @return inputNumber in british english words
     * @throws IllegalArgumentException if inputNumber is too big.
     */
    public String transform(int inputNumber) throws IllegalArgumentException;

}
