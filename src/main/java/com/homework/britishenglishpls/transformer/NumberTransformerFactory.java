package com.homework.britishenglishpls.transformer;

import com.homework.britishenglishpls.transformer.en.BritishNumberTransformer;

public class NumberTransformerFactory {

    public static NumberTransformer getTransformer(LANG lang) {
        switch (lang) {
            case EN:
                return new BritishNumberTransformer();
        }
        return null;
    }

    public enum LANG { EN };

}
